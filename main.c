#include<stdio.h>
#define __USE_XOPEN2K
#include<pthread.h>

pthread_barrier_t barrier;
void* lcd(void* data)
{
	printf("lcd: inside critical section \n");
	pthread_barrier_wait(&barrier);
	sleep(5);
	printf("lcd: outside critical section \n");
}

void* serial(void* data)
{
	printf("serial: inside critical section \n");
	pthread_barrier_wait(&barrier);
	sleep(7);
	printf("Serial: outside critical section \n");
}

void* update(void* data)
{
	printf("update: inside critical section \n");
	pthread_barrier_wait(&barrier);
	sleep(5);
	printf("update: outside critical section \n");
}

int main()
{
//	while(1)
//	{
		pthread_t idlcd,idserial,idupdate;
	
		pthread_barrier_init(&barrier,NULL,3);
		pthread_create(&idlcd,NULL,lcd,NULL);
		pthread_create(&idserial,NULL,serial,NULL);
		pthread_create(&idupdate,NULL,update,NULL);
	
		pthread_join(&idlcd,NULL);
		pthread_join(&idserial,NULL);
		pthread_join(&idupdate,NULL);
		pthread_barrier_destroy(&barrier);
//	}
	return 0;
}

